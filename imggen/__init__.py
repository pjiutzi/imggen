"""
__init__
~~~~~~~~

Initialization for the imggen package.
"""
__all__ = ['maze', 'noise', 'patterns', 'perlin', 'unitnoise', 'worley']
