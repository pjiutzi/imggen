"""
__version__
~~~~~~~~~~~

Common information about the imggen module, including the version
number.
"""

__title__ = 'imggen'
__description__ = 'Procedural image data creation.'
__version__ = '0.1.1'
__author__ = 'Paul J. Iutzi'
__license__ = 'MIT'
__copyright__ = 'Copyright (c) 2021 Paul J. Iutzi'
